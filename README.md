# Cpanel #

### Usage: ###  
 * To create subdomain and database in Cpanel  
 http://{cpanel.dev}/cpanel.php?create_subdomain=yes&subdomain={subdomain_name}&create_db=yes&cp_db={db_name}&debug  
 * To create only subdomain in Cpanel  
 http://{cpanel.dev}/cpanel.php?create_subdomain=yes&subdomain={subdomain_name}&debug  
 * To create only database in Cpanel  
 http://{cpanel.dev}/cpanel.php?create_db=yes&cp_db={db_name}&debug