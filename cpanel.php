<?php
/**
 * Usage:
 * To create subdomain and database in Cpanel
 * http://cpanel.dev/cpanel.php?create_subdomain=yes&subdomain={subdomain_name}&create_db=yes&cp_db={db_name}&debug
 * To create only subdomain in Cpanel
 * http://cpanel.dev/cpanel.php?create_subdomain=yes&subdomain={subdomain_name}&debug
 * To create only database in Cpanel
 * http://cpanel.dev/cpanel.php?create_db=yes&cp_db={db_name}&debug
 */

require 'capi/xmlapi.php';
require 'capi/functions.php';
$break = '<br/>';

$xmlapi = new xmlapi(ROOT_DOMAIN);
$xmlapi->set_port(2083);
$cpaneluser = CP_USER;
$cpanelpass = CP_PASS;

$xmlapi->password_auth($cpaneluser, $cpanelpass);
$xmlapi->set_debug(1); //output actions in the error log 1 for true and 0 false

/*Cpanel Database create code*/
if (isset($_GET['create_db']) && $_GET['create_db'] == 'yes') {

    if (!isset($_GET['cp_db']) || $_GET['cp_db'] == '') {
        echo "Cpanel database name is required";
        exit;
    }

    $databasename = $_GET['cp_db'];
    $databaseuser = 'devzone_admin';

    //create database
    $createdb = $xmlapi->api1_query($cpaneluser, "Mysql", "adddb", array($databasename));
    //create user
    //$usr = $xmlapi->api1_query($cpaneluser, "Mysql", "adduser", array($databaseuser, $databasepass));
    //add user
    $addusr = $xmlapi->api1_query($cpaneluser, "Mysql", "adduserdb", array($databasename, $databaseuser, 'all'));

    if (isset($_GET['debug'])) {
        trace($createdb);
        trace($addusr);
    }

    echo "Cpanel DB should be created." . $break;
}

/*Cpanel subdomain create code*/
if (isset($_GET['create_subdomain']) && $_GET['create_subdomain'] == 'yes') {

    if (!isset($_GET['subdomain']) || $_GET['subdomain'] == '') {
        echo "Cpanel subdomain name is required";
        exit;
    }

    $domainroot    = ROOT_DOMAIN;
    $subdomain_dir_name     = $_GET['subdomain'] . '.' . $domainroot;
    $subdomain     = $_GET['subdomain'];
    $add_subdomain = $xmlapi->api1_query($cpaneluser, 'SubDomain', 'addsubdomain', array($subdomain, $domainroot, 0, 0, "/sites/{$subdomain_dir_name}"));

    if (isset($_GET['debug'])) {
        trace($add_subdomain);
    }

    echo "Cpanel subdomain should be created." . $break;
}

echo "End of script." . $break;
